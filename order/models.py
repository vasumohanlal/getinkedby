from datetime import date

from django.db import models
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context

import getinkedby.settings as settings
from getinkedby import code_generator
from registration.models import Studio
from product.models import Product, ProductConfiguration

# Create your models here.
class Order(models.Model):
    txn_id = models.CharField("Transaction ID", max_length=25)
    txn_status = models.CharField("Status", max_length=25, db_index=True)
    price = models.CharField("Price", max_length=25, null=True)
    studio = models.ForeignKey(Studio, db_index=True, null=True)
    product = models.ForeignKey(ProductConfiguration, db_index=True, null=True)
    code = models.CharField("Code", max_length=12, db_index=True, unique=True, default='')
    claimed = models.BooleanField("Claimed", default=False)
    claimedon = models.DateField("Claimed On", null=True)
    expiry = models.DateField(null=True)
    buyer = models.CharField("Buyer", max_length=75,)
    email = models.EmailField("Email", db_index=True)
    address =  models.TextField("Address", max_length=250, null=True)
    created = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'inkedby_orders'
        unique_together = ('txn_id', 'product', 'studio', 'code',)

    def __unicode__(self):
        return "%s : %s" % (self.txn_id, self.code)

    def save(self, *args, **kwargs):
        if not self.code:
            existing_codes = [order.code for order in Order.objects.all().order_by('code')]
            while True:
                self.code = code_generator(size=12)
                if self.code not in existing_codes:
                    break
        self.claimedon = self.claimed and date.today() or None
        super(Order, self).save(*args, **kwargs)
    
    def send_email(self, textc, htmlc, subject, to):
        d = Context({ 'order': self })
        text_content = textc.render(d)
        html_content = htmlc.render(d)
        msg = EmailMultiAlternatives(subject, text_content, settings.DEFAULT_FROM_EMAIL, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    
    def send_studio(self):
        text_content = get_template('registration/studio.txt')
        html_content = get_template('registration/studio.html')
        self.send_email(text_content, html_content, settings.ORDER_SUBJECT, self.studio.email )
    
    def send_buyer(self):
        text_content = get_template('registration/buyer.txt')
        html_content = get_template('registration/buyer.html')
        subject = settings.ORDER_SUBJECT + " from %s" % self.studio.name
        self.send_email(text_content, html_content, subject, self.email)
    
    def send_claim(self):
        text_content = get_template('registration/claim.txt')
        html_content = get_template('registration/claim.html')
        subject = settings.CLAIM_SUBJECT + " from %s" % self.studio.name
        if self.claimedon:
            self.send_email(text_content, html_content, subject, settings.CLAIM_ADDRESS)