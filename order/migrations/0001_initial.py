# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
        ('registration', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('txn_id', models.CharField(max_length=25, verbose_name=b'Transaction ID')),
                ('txn_status', models.CharField(max_length=25, verbose_name=b'Status', db_index=True)),
                ('price', models.CharField(max_length=25, null=True, verbose_name=b'Price')),
                ('code', models.CharField(default=b'', unique=True, max_length=12, verbose_name=b'Code', db_index=True)),
                ('claimed', models.BooleanField(default=False, verbose_name=b'Claimed')),
                ('claimedon', models.DateField(null=True, verbose_name=b'Claimed On')),
                ('expiry', models.DateField(null=True)),
                ('buyer', models.CharField(max_length=75, verbose_name=b'Buyer')),
                ('email', models.EmailField(max_length=75, verbose_name=b'Email', db_index=True)),
                ('address', models.TextField(max_length=250, null=True, verbose_name=b'Address')),
                ('created', models.DateField(auto_now_add=True)),
                ('product', models.ForeignKey(to='product.ProductConfiguration', null=True)),
                ('studio', models.ForeignKey(to='registration.Studio', null=True)),
            ],
            options={
                'db_table': 'inkedby_orders',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='order',
            unique_together=set([('txn_id', 'product', 'studio', 'code')]),
        ),
    ]
