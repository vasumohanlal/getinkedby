from django.contrib import admin

from models import Order

# Register your models here.
@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('txn_id', 'txn_status', 'product', 'price', 'code', 'studio', 'claimed', 'claimedon', 'buyer', 'email')
    list_filter = ('claimed', 'expiry', 'created')
    search_fields = ('studio', 'code', 'txn_id')
    readonly_fields = ('code', 'price', 'txn_id', 'product', 'studio', 'txn_status', 'buyer', 'email', 'address', 'created', 'claimed', 'claimedon')
    fieldsets = [
    	('Transaction', {'fields':['txn_id', 'txn_status', 'product', 'price', 'created']}),
        ('Redemption', {'fields':['studio', 'code', 'expiry', 'claimed', 'claimedon']}),
	    ('Buyer', {'fields':['buyer', 'email', 'address']}),
    ]

