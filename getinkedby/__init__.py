import string
import random

def code_generator(size=16, chars=string.ascii_uppercase+string.digits):
    return ''.join(random.sample(chars*size, size))