from django.db import models

from getinkedby import code_generator
from registration.models import Studio

# Create your models here.
class Product(models.Model):
    product_type = models.CharField("Type", max_length=20, db_index=True, unique=True)
    description = models.CharField("Description", max_length=250, blank=True)
    currency = models.CharField("Currency", max_length=10)
    created = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'inkedby_products'
        ordering = ['product_type']

    def __unicode__(self):
        return self.product_type

class ProductConfiguration(models.Model):
    studio = models.ForeignKey(Studio, db_index=True)
    product = models.ForeignKey(Product, db_index=True)
    value = models.DecimalField("Value", max_digits=6, decimal_places=2)
    discount = models.DecimalField("Discount", max_digits=5, decimal_places=2, help_text="This field value is always in percentage")
    code = models.CharField("Code", unique=True, max_length=10, editable=False, default='', db_index=True)

    class Meta:
        db_table = 'inkedby_product_configuration'
        ordering = ['studio', 'product', 'code']
        unique_together = ('studio','product', 'code')

    def __unicode__(self):
        return "%s : %s" % (self.product.product_type, self.code)
    
    def save(self, *args, **kwargs):
        if not self.code:
            existing_codes = [prod_config.code for prod_config in ProductConfiguration.objects.all().order_by('code')]
            while True:
                self.code = code_generator(size=10)
                if self.code not in existing_codes:
                    break
        super(ProductConfiguration, self).save(*args, **kwargs)
