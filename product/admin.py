from django.contrib import admin

from models import Product, ProductConfiguration

# Register your models here.
class ProductConfigurationInline(admin.TabularInline):
    model = ProductConfiguration
    extra = 1

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['product_type', 'description', 'currency', 'created',]
    search_fields = ['product_type']
    fieldsets = [
        ('Product', {'fields':['product_type', 'description', 'currency']}),
    ]
    inlines = [ProductConfigurationInline]

