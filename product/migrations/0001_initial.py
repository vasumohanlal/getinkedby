# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_type', models.CharField(unique=True, max_length=20, verbose_name=b'Type', db_index=True)),
                ('description', models.CharField(max_length=250, verbose_name=b'Description', blank=True)),
                ('currency', models.CharField(max_length=10, verbose_name=b'Currency')),
                ('created', models.DateField(auto_now_add=True)),
            ],
            options={
                'ordering': ['product_type'],
                'db_table': 'inkedby_products',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.DecimalField(verbose_name=b'Value', max_digits=6, decimal_places=2)),
                ('discount', models.DecimalField(help_text=b'This field value is always in percentage', verbose_name=b'Discount', max_digits=5, decimal_places=2)),
                ('code', models.CharField(default=b'', editable=False, max_length=10, unique=True, verbose_name=b'Code', db_index=True)),
                ('product', models.ForeignKey(to='product.Product')),
                ('studio', models.ForeignKey(to='registration.Studio')),
            ],
            options={
                'ordering': ['studio', 'product', 'code'],
                'db_table': 'inkedby_product_configuration',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='productconfiguration',
            unique_together=set([('studio', 'product', 'code')]),
        ),
    ]
