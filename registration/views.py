import urllib
import urllib2
from datetime import date, timedelta

from django.shortcuts import render, get_object_or_404
from django.http import Http404, JsonResponse
from django.views.decorators.clickjacking import xframe_options_exempt
from django import forms

import getinkedby.settings as settings
from models import Studio
from order.models import Order
from product.models import Product, ProductConfiguration

# Create your views here.
def index(request, *args, **kwargs):
    return render(request, 'registration/index.html', {})

def home(request, *args, **kwargs):
    studio = get_object_or_404(Studio, instagram=kwargs.get('instagram', None))
    return render(request, 'registration/cover.html', {'studio':studio})

@xframe_options_exempt
def shop(request, *args, **kwargs):
    studio = get_object_or_404(Studio, instagram=kwargs.get('instagram', None))
    return render(request, 'registration/shop.html', {'studio':studio})
	
@xframe_options_exempt
def widget(request, *args, **kwargs):
    studio = get_object_or_404(Studio, instagram=kwargs.get('instagram', None))
    return render(request, 'registration/widget.html', {'studio':studio})

def product(request, *args, **kwargs):
    try:
        studio = Studio.objects.get(instagram=kwargs.get('instagram', None))
    except Studio.DoesNotExist:
        studio = None
    try:
        product = Product.objects.get(product_type=kwargs.get('product', None))
    except Product.DoesNotExist:
        product = None
    if product:
        configs = ProductConfiguration.objects.filter(studio=studio, product=product)
    else:
        configs = ProductConfiguration.objects.filter(studio=studio)
    data = {}
    if studio:
        data['username'] = studio.instagram
        data['studio'] = studio.name
        data['location'] = ', '.join([branch.city.capitalize() for branch in studio.branch_set.all()])
        if configs:
            data.setdefault('configs', [])
            for config in configs:
                data['configs'].append((config.product.product_type, config.code, config.product.currency, config.value, config.discount))
    return JsonResponse(data)

def validate(request, *args, **kwargs):
    if request.method == 'POST':
        data = {}
        redemption_code = request.POST.get('redemption_code')
        studio_code = request.POST.get('studio_code')
        action = request.POST.get('action')
        data['redemption_code'] = redemption_code
        data['studio_code'] = studio_code
        data['action'] = action
        if redemption_code and not studio_code:
            try:
                order = Order.objects.get(code=redemption_code)
                data['status'] = 0
                if not order.claimed and order.expiry < date.today():
                    data['error'] = "This redemption code is already expired."
                    data['status'] = 1
                elif order.claimed:
                    data['status'] = 1
                    data['error'] = "This redemption code is already claimed."
            except:
                order = None
                data['error'] = 'Invalid redemption code'
                data['status'] = 1
        elif redemption_code and studio_code and not action:
            try:
                order = Order.objects.get(code=redemption_code)
            except:
                data['error'] = 'Invalid redemption code'
                data['status'] = 1                
            else:
                if order.studio.code == studio_code:
                    data['status'] = 0
                    if not order.claimed and order.expiry < date.today():
                        data['error'] = "This redemption code is already expired."
                        data['status'] = 1
                    elif order.claimed:
                        data['status'] = 1
                        data['error'] = "This redemption code is already claimed."
                    else:
                        data.update(get_order_details(order))
                else:
                    data['error'] = "Invalid studio code"
                    data['status'] = 1
        elif redemption_code and studio_code and action=='claim':
            try:
                order = Order.objects.get(code=redemption_code)
            except:
                data['error'] = 'Invalid redemption code'
                data['status'] = 1
            else:
                if order.studio.code == studio_code:
                    data['status'] = 0
                    if not order.claimed and order.expiry < date.today():
                        data['error'] = "This redemption code is already expired."
                        data['status'] = 1
                    elif order.claimed:
                        data['status'] = 1
                        data['error'] = "This redemption code is already claimed."
                    elif not order.claimed:
                        order.claimed = True
                        order.save()
                        order.send_claim()
                else:
                    data['error'] = "Invalid studio code"
                    data['status'] = 1
        else:
            data['error'] = 'Invalid request'
            data['status'] = 1
        return JsonResponse(data)                
    if request.method == 'GET':
        return render(request, 'registration/validate.html', {})

def convert2unicode(text, charset='UTF-8'):
    if not isinstance(text, unicode):
        return urllib.unquote_plus(text).decode(charset).encode('utf-8')
    return urllib.unquote_plus(text)

def get_order_details(order):
    data = {}
    data['txn_id'] = order.txn_id
    data['txn_status'] = order.txn_status
    data['price'] = order.price
    data['payment'] = "%s" % order.created
    data['code'] = order.code
    data['expiry'] = "%s" % order.expiry
    data['buyer'] = order.buyer
    data['email'] = order.email
    data['currency'] = order.product.product.currency
    return {'order': data}    
    
def payment(request, *args, **kwargs):
    tx = request.GET.get('tx')
    params = {'cmd':'_notify-synch',
              'tx': tx,
              'at':settings.PAYPAL_PDT_IDENTITY_TOKEN
              }
    encoded_params = urllib.urlencode(params)
    pay_req = urllib2.Request(settings.PAYPAL_PDT_POSTBACK, encoded_params)
    pay_response = urllib2.urlopen(pay_req)
    response_lines = pay_response.readlines()
    status =  response_lines[0].strip().upper()
    transaction_details =  dict([line.strip().split('=', 1) for line in response_lines[1:] if line and line.strip() and len(line.strip().split('=', 1)) == 2])
    studio = buyer = None
    if status == 'SUCCESS':
        charset = transaction_details.get('charset', 'UTF-8')
        firstname = convert2unicode(transaction_details.get('first_name'), charset)
        lastname = convert2unicode(transaction_details.get('last_name'), charset)
        buyer = "%s %s" % (firstname.capitalize(), lastname.capitalize())
        try:
            studio = Studio.objects.get(instagram=transaction_details.get('custom'))
        except Studio.DoesNotExist:
            studio = ''
        else:
            cart_items = int(transaction_details.get('num_cart_items', '0'))
            if cart_items:
                email = urllib.unquote(transaction_details.get('payer_email'))
                street = convert2unicode(transaction_details.get('address_street'), charset)
                city = convert2unicode(transaction_details.get('address_city'), charset)
                country = convert2unicode(transaction_details.get('address_country'), charset)
                zip = convert2unicode(transaction_details.get('address_zip'), charset)
                expiry = date.today() + timedelta(days=365)
                for idx in range(1, cart_items+1):
                    itemcode = transaction_details.get('item_name%d' % idx)
                    quantity = int(transaction_details.get('quantity%d' % idx))
                    price = float(transaction_details.get('mc_gross_%d' % idx))/quantity
                    product = ProductConfiguration.objects.get(code=itemcode, studio=studio)
                    db_list = Order.objects.filter(txn_id=tx, txn_status=status, product=product, studio=studio)
                    if len(db_list) == quantity:
                        continue
                    for qidx in range(1, quantity+1):
                        order = Order(txn_id=tx, txn_status=status, price=price, product=product, studio=studio)
                        order.expiry = expiry
                        order.buyer = buyer
                        order.email = email
                        order.address = "%s,\n%s,\n%s,\n%s." % (street, city, country, zip)
                        order.save()
                        order.send_studio()
                        order.send_buyer()
    return render(request, 'registration/payment.html', {'status': status, 'studio': studio, 'buyer': buyer, 'txn_id': tx})