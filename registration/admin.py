from django.contrib import admin

from models import Studio, Branch
from product.models import ProductConfiguration

# Register your models here.
class BranchInline(admin.TabularInline):
    model = Branch
    extra = 1

class ProductConfigurationInline(admin.TabularInline):
    model = ProductConfiguration
    extra = 1

class StudioAdmin(admin.ModelAdmin):
    list_display = ['name','email', 'instagram', 'fb', 'reviews','code']
    search_fields = ['name', 'email', 'instagram', 'code']
    list_filter = ['created']
    readonly_fields = ('code',)
    fieldsets = [
	('Studio', {'fields':['name', 'email', 'mobile', 'cover', 'code',]}),
	('Social', {'fields':['instagram', 'fb', 'reviews']}),
    ]
    inlines = [BranchInline, ProductConfigurationInline]

admin.site.register(Studio, StudioAdmin)
