from django.conf.urls import patterns, include, url
from views import index, validate, home, widget, product, payment, shop

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from getinkedby import settings 

urlpatterns = patterns('',
    url(r'^$', index, name='index'),
    url(r'^validate/$', validate, name='validate'),
    url(r'^payment/$', payment, name='payment'),
    url(r'^(?P<instagram>\w+)/$', home, name='home'),
	url(r'^(?P<instagram>\w+)/shop/$', shop, name='shop'),
    url(r'^(?P<instagram>\w+)/widget/$', widget, name='widget'),
    url(r'^(?P<instagram>\w+)/product/((?P<product>[ \w]+)/)?$', product, name='product'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
