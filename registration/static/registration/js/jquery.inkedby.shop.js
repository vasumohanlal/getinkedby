/*
jQuery plugin: inkedby
author: Parthiban Ramasamy

reference: http://www.smashingmagazine.com/2011/10/11/essential-jquery-plugin-patterns/
*/
;(function ( $, window, document, undefined ) {
	var pluginName = 'GetInkedby',
		defaults = {
			username: null,
			product:null,
		}
	
	function GetInkedby(element, options){
		this.element = element;
		this.options = $.extend( {}, defaults, options) ;
        
        this._defaults = defaults;
        this._name = pluginName;
		this.apiurl = "https://api.instagram.com/v1/";
		this.baseurl = "http://get.inked.by/";
		this.userID = null;
		this.profile_picture = null;
		this.website = null;
		this.pictures = [];
		this.clientID = "6d9d24a26adf4494a4a8f8651b35fe53";
		this.init();
	}
	
	GetInkedby.prototype = {
        init: function() {
			if (this.options.username){
				$(this.element).addClass("container");
				$(this.element).append("<div class=\"md-modal\" id=\"modal\"><div class=\"md-content\"></div></div>");
				$(this.element).append("<script type=\"text/javascript\" src=\""+this.baseurl+"static/registration/js/simpleCart.js\"></script>");
				this.sendRequest(this.apiurl + "users/search", {q:this.options.username, client_id: this.clientID, count:1}, this.callbackUsername, 'jsonp');
			}
			else {
				$(this.element).append("<span style=\"color:red;\">Not valid user. Please email weareflipverse@gmail.com</span>")
			}
		},

		sendRequest: function(url, params, callback, datatype) {
			var o = this;
			$.ajax({
				method: "GET",
				url: url,
				data: params,
				dataType: datatype,
				success: function(data) {
					callback(data, o);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
		},

		callbackUsername: function(data, o) {
			if(data.data.length > 0) 
			{
				o.profile_picture = data.data[0].profile_picture;
				o.website = data.data[0].website;
				if (o.options.product)
					o.sendRequest(o.baseurl + o.options.username + "/product/" + o.options.product + "/", {}, o.callbackOrderForm, 'json');
				else
					o.sendRequest(o.baseurl + o.options.username + "/product/", {}, o.callbackOrderForm, 'json');
			}
			else
			{
				alert("No user was found");
			}
		},

		callbackOrderForm: function(data, o)
		{
			if(data.configs.length > 0) 
			{
				$(o.element).append("<script type='text/javascript'>simpleCart = new cart('info@inkmedia.se','"+o.options.username+"');" +
						"simpleCart.currency='SEK';createCart();</script>");
				var studio = data.studio;
				var location = data.location;
                content = $(o.element).find(".md-content");
                content.append("<div class='inkedby-cart-btn hidden-desktop visible-phone'><a href='#' onclick='$(\".simpleCart_items\").toggle();return false;'><i class='fa fa-fw fa-border fa-shopping-cart'></i><span class='simpleCart_quantity'></span></a></div>" +
                		"<h3>Buy Gift Card</h3><div><div style='height:30px'><div style='float:left'><p class='hidden-phone'>Select the package which you want to purchase:</p></div>" +
                		"<div class='inkedby-cart-btn hidden-phone'><a href='#' onclick='$(\".simpleCart_items\").toggle();return false;'><i class='fa fa-fw fa-border fa-shopping-cart'></i><span class='simpleCart_quantity'></span></a></div>" +
                		"<div style='clear:both'></div><div class='simpleCart_items'></div></div>" +
                		"<table class='inkedby-ordertable'>" +
                		"<thead><tr><th>ITEM</th><th class='hidden-phone'>VALUE</th><th class='hidden-phone'>% DISCOUNT</th><th>PRICE</th><th></th></tr></thead>" +
                		"<tbody></tbody></table>" +
                		"<footer class='post-footer'><div itemprop='author' itemscope='' itemtype='http://schema.org/Person' class='post-author'>" +
                		"<a href='"+o.website+"' class='post-author-avatar'><img itemprop='image' src='"+o.profile_picture+"'></a>" +
                		"<div class='post-author-info'><h4 class='post-footer-heading'>Get.Inked.By</h4>" +
                		"<a href='"+o.website+"' itemprop='url' class='post-author-name'><span itemprop='name'>"+studio+"</span></a>" +
                		"<small class='post-author-location'>"+location+"</small></div></div><div class='post-social'>" +
                		"<h4 class='post-footer-heading'>Spread the word</h4><a href='#' data-action='share-twitter'><i class='fa fa-fw fa-lg fa-twitter'></i></a>" +
                		"<a href='#' data-action='share-facebook'><i class='fa fa-fw fa-lg fa-facebook'></i></a>" +
                		"<a href='#' data-action='share-gplus'><i class='fa fa-fw fa-lg fa-google-plus'></i></a></div></footer>" +
                		"<footer class='footer'><p><small>&copy; 2014. All Rights Reserved. Proudly published with <a href='http://inked.by' target='_blank'>Inked.by</a></small></p></footer>");
                tbody = $(o.element).find(".inkedby-ordertable tbody");
				for (config in data['configs']) {
					var type = data['configs'][config][0]
					var code = data['configs'][config][1]
					var currency = data['configs'][config][2]
					var value = parseFloat(data['configs'][config][3]).toFixed(2)
					var discount = data['configs'][config][4]
					var price = value-value*parseFloat(discount)/100
					tbody.append("<tr class='simpleCart_shelfItem'><td><p>"+type+"</p><small class='hidden-phone'>"+code+"</small></td>"+
							"<td class='hidden-phone'>"+value+" "+currency+"</td><td class='hidden-phone'>"+discount+"%</td><td><span>"+price.toFixed(2)+"</span> "+currency+"</td>" +
							"<td><button class='hidden-phone' onclick=\"simpleCart.add('name="+code+"','price="+price.toFixed(2)+"');return false;\"" +
							">Add to Cart</button><button class='visible-phone hidden-desktop' onclick=\"simpleCart.add('name="+code+"','price="+price.toFixed(2)+"');return false;\"" +
							">+ Add</button></td></tr>");
				}
				$(o.element).append("<script type='text/javascript'>$('.post-social a').click(function(){ "+
						"if ($(this).attr('data-action') == 'share-gplus'){var url=\"https://plus.google.com/share?url="+encodeURIComponent(window.location.href)+"\";" +
						"var opt=\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=600,height=600\";}"+
						"if ($(this).attr('data-action') == 'share-facebook'){var url=\"https://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent(window.location.href)+"\";"+
						"var opt=\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=436,height=626\";}"+
						"if ($(this).attr('data-action') == 'share-twitter'){var url=\"https://twitter.com/share?url="+encodeURIComponent(window.location.href)+"\";"+
						"var opt=\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=440,height=540\";}"+
						"window.open(url,'', opt); return false; })</script>")
			}
		},
	};
	
	$.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, 
                new GetInkedby( this, options ));
            }
        });
    }

})( jQuery, window, document );