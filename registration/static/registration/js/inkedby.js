function widget() {
	container = document.getElementById("inkedby-container");
	username = container.getAttribute("rel");
	product = container.getAttribute("product");
	document.title = 'Get inked by ' + username;

	var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = 'http://get.inked.by/static/registration/css/inkedby-widget.css';
    document.head.appendChild(link);	
	var script1 = document.createElement('script');
    script1.type = 'text/javascript';
    script1.src = 'http://get.inked.by/static/registration/js/jquery-1.7.1.min.js';
    document.head.appendChild(script1);

    var script2 = document.createElement('script');
    script2.type = 'text/javascript';
    script2.src = 'http://get.inked.by/static/registration/js/jquery.inkedby.js';
    document.body.appendChild(script2);
    var script3 = document.createElement('script');
    script3.type = 'text/javascript';
    if (!product) {
    	script3.innerHTML = "$(document).ready(function() {$('#inkedby-container').GetInkedby({username:'"+username+"'});});";
    }
    else {
    	script3.innerHTML = "$(document).ready(function() {$('#inkedby-container').GetInkedby({username:'"+username+"', product:'"+product+"'});});";
    }
    document.body.appendChild(script3);
}
window.onload = widget;