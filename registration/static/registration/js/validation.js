jQuery(document).ready(function($){
	var baseurl = "http://get.inked.by/";
	$('.first-form').show();
	$('.second-form').hide();
	$('.third-form').hide();
	$('#redemption_code').attr('maxlength', '12');
	$('#studio_code').attr('maxlength', '5');
	$('#redemption_code').keyup(function(){
		if ($(this).val().length == 12){
			sendRequest(baseurl + "validate/", {redemption_code:$(this).val()}, callRedemption, 'json');
		}
	});
	$('#studio_code').keyup(function(){
		if ($(this).val().length == 5){
			sendRequest(baseurl + "validate/", {studio_code:$(this).val(), redemption_code:$('#redemption_code').val() }, callStudio, 'json');
		}
	});
	$('.form1-button').click(function(){
		$('.second-form').show();
		$('.first-form').hide();
	});
	$('.form2-button').click(function(){
		sendRequest(baseurl + "validate/", {studio_code:$('#studio_code').val(), redemption_code:$('#redemption_code').val(), action:'claim'}, callThanks, 'json');		
	});
	function sendRequest(url, params, callback, datatype) {
		params.csrfmiddlewaretoken = $("input[name='csrfmiddlewaretoken']").val()
		$.ajax({
			type: "POST",
			url: url,
			data: params,
			dataType: datatype,
			success: function(data) {
				callback(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	};
	function callRedemption(data) {
		if (!data.status){
			$('.form1-button').css('display', 'block');
			$('.message1 .annotation1').text('Click the next button to claim this redemption code.');
			$('.message1 .error1').text('');
		}
		else{
			$('.form1-button').css('display', 'none')
			$('.message1 .error1').text(data.error);
			$('.message1 .annotation1').text('');
		}
	};
	function callStudio(data) {
		if (!data.status){
			$('.form2-button').css('display', 'block');
			$('.message2 .error2').text('');
			$('.details').css('display', 'block');
			$('#txn_id').text(data.order.txn_id);
			$('#txn_status').text(data.order.txn_status);
			$('#price').text(data.order.price+" "+data.order.currency);
			$('#payment').text(data.order.payment);
			$('#buyer').text(data.order.buyer);
			$('#email').text(data.order.email);
			$('#code').text(data.order.code);
			$('#expiry').text(data.order.expiry);
			$('.message2 .annotation2').text('Click the claim button to accept this redemption code.');
		}
		else{
			$('.form2-button').css('display', 'none')
			$('.message2 .error2').text(data.error);
			$('.message2 .annotation2').text('');
			$('.details').css('display', 'none');
			$('#txn_id').text('');
			$('#txn_status').text('');
			$('#price').text('');
			$('#payment').text('');
			$('#buyer').text('');
			$('#email').text('');
			$('#code').text('');
			$('#expiry').text('');
		}
	};
	function callThanks(data){
		$('.second-form').hide();
		$('.first-form').hide();
		if (!data.status){
			$('.error3').text('');
			$('.thanks').text('Thanks for accepting the '+data.redemption_code+' code');
		}
		else{
			$('.error3').text(data.error);
			$('.thanks').text('');
		}
	};
});