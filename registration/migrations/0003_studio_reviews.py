# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0002_auto_20141230_1140'),
    ]

    operations = [
        migrations.AddField(
            model_name='studio',
            name='reviews',
            field=models.DecimalField(default=0, verbose_name=b'Reviews', max_digits=2, decimal_places=1),
            preserve_default=True,
        ),
    ]
