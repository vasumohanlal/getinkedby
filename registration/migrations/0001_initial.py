# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Branch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.CharField(max_length=250, verbose_name=b'Address')),
                ('city', models.CharField(max_length=25, verbose_name=b'City')),
                ('country', models.CharField(max_length=25, verbose_name=b'Country')),
                ('zip_code', models.IntegerField(max_length=10, verbose_name=b'Zip code')),
            ],
            options={
                'db_table': 'inkedby_studio_branches',
                'verbose_name': 'Branch',
                'verbose_name_plural': 'Branches',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Studio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Studio Name', db_index=True)),
                ('instagram', models.CharField(max_length=50, verbose_name=b'Instagram ID', db_index=True)),
                ('email', models.EmailField(max_length=75, verbose_name=b'Email', db_index=True)),
                ('mobile', models.IntegerField(max_length=16, null=True, verbose_name=b'Mobile Number')),
                ('code', models.CharField(default=b'', editable=False, max_length=5, unique=True, verbose_name=b'Studio code', db_index=True)),
                ('created', models.DateField(auto_now_add=True)),
            ],
            options={
                'ordering': ['name', 'instagram'],
                'db_table': 'inkedby_studios',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='studio',
            unique_together=set([('name', 'instagram', 'email', 'code')]),
        ),
        migrations.AddField(
            model_name='branch',
            name='studio',
            field=models.ForeignKey(to='registration.Studio'),
            preserve_default=True,
        ),
    ]
