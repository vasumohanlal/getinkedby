# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='studio',
            name='fb',
            field=models.URLField(default=b'', verbose_name=b'Facebook URL', db_index=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='studio',
            name='mobile',
            field=models.IntegerField(max_length=16, null=True, verbose_name=b'Mobile Number', blank=True),
            preserve_default=True,
        ),
    ]
