# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0004_studio_cover'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studio',
            name='cover',
            field=models.ImageField(default=b'cover/inkedby.jpg', upload_to=b'cover/%Y/%m/%d', blank=True, help_text=b'Please use JPG/JPEG format with max 1MB in size and min 800x600 pixels', verbose_name=b'Cover Photo'),
            preserve_default=True,
        ),
    ]
