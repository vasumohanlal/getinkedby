import string

from django.core.exceptions import ValidationError
from django.db import models

from getinkedby import code_generator

# Create your models here.
class Studio(models.Model):
    name = models.CharField("Studio Name", max_length=50, db_index=True)
    email = models.EmailField("Email", db_index=True)
    mobile = models.IntegerField("Mobile Number", max_length=16, null=True, blank=True)
    instagram = models.CharField("Instagram ID", max_length=50, db_index = True)
    fb = models.URLField("Facebook URL", max_length=200, db_index = True, blank=True, default='')
    code = models.CharField("Studio code", unique=True, max_length=5, editable=False, default='', db_index=True)
    created = models.DateField(auto_now_add=True)
    reviews = models.DecimalField("Reviews", max_digits=2, decimal_places=1, default=0)
    cover = models.ImageField("Cover Photo", blank=True, default='cover/inkedby.jpg', upload_to='cover/%Y/%m/%d',
                              help_text="Please use JPG/JPEG format with max 1MB in size and min 800x600 pixels")

    class Meta:
        db_table = 'inkedby_studios'
        unique_together = ('name', 'instagram', 'email', 'code')
        ordering = ['name', 'instagram']

    def __unicode__(self):
        return "%s : %s" % (self.name, self.instagram)
    
    def clean(self, *args, **kwargs):
        if self.cover:
            ext = self.cover.name.split('.')[-1]
            if  ext.lower() not in ['jpg', 'jpeg']:
                raise ValidationError({'cover':"Please upload only JPEG/JPG formatted photos"})
            if self.cover.size > 1*1024*1024:
                raise ValidationError({'cover':"Image file too large ( maximum 1MB )"})
            if self.cover.height < 600 or self.cover.width < 800:
                raise ValidationError({'cover':"Image dimensions are not valid, dimensions atleast to be 800x600 pixels )"})
        else:
            raise ValidationError({'cover':"Couldn't read uploaded image"})

    def save(self, *args, **kwargs):
        if not self.code:
            existing_codes = [studio.code for studio in Studio.objects.all().order_by('code')]
            while True:
                self.code = code_generator(size=5)
                if self.code not in existing_codes:
                    break
        super(Studio, self).save(*args, **kwargs)

class Branch(models.Model):
    studio = models.ForeignKey(Studio)
    address =  models.CharField("Address", max_length=250)
    city = models.CharField("City", max_length=25)
    country = models.CharField("Country", max_length=25)
    zip_code = models.IntegerField("Zip code", max_length=10)

    class Meta:
        db_table = 'inkedby_studio_branches'
        verbose_name = 'Branch'
        verbose_name_plural = 'Branches'

    def __unicode__(self):
        return "%s @%s" % (self.studio.name, self.city)
